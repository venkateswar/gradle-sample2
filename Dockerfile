FROM openjdk:8
VOLUME /tmp
COPY /build/libs/gradle-sample-0.0.1-SNAPSHOT.jar  app.jar
EXPOSE 8080
CMD ["java","-jar","gradle-sample-0.0.1-SNAPSHOT.jar"]
